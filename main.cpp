#pragma clang diagnostic push
#pragma ide diagnostic ignored "modernize-use-nodiscard"

#include <iostream>
#include <vector>
#include <memory>

using namespace std;

class IRareCoin
{
public:
    virtual string getName() const = 0;

    virtual size_t getPrice() const = 0;

    virtual size_t getRealQuantity() const = 0;

    virtual size_t getDocumentedQuantity() const = 0;
};

class RareCoin : public IRareCoin
{
private:
    string _name;
    size_t _price;
    size_t _real_quantity;
    size_t _documented_quality;
public:
    RareCoin(string name, size_t price, size_t real_quantity, size_t documented_quality) :
            _name(std::move(name)), _price(price), _real_quantity(real_quantity),
            _documented_quality(documented_quality)
    {}

    string getName() const override
    { return _name; }

    size_t getPrice() const override
    { return _price; }

    size_t getRealQuantity() const override
    { return _real_quantity; }

    size_t getDocumentedQuantity() const override
    { return _documented_quality; }
};

class IReportEntry
{
public:
    virtual string getName() = 0;

    virtual size_t getEachPrice() = 0;

    virtual size_t getStolenQuantity() = 0;

    size_t getTotalPrice();
};

class ReportEntry : public IReportEntry
{
private:
    string _name;
    size_t _each_price;
    size_t _stolen_quantity;

public:
    ReportEntry(string name, size_t each_price, size_t stolen_quantity) :
            _name(std::move(name)), _each_price(each_price), _stolen_quantity(stolen_quantity)
    {}

    string getName() override
    { return _name; }

    size_t getEachPrice() override
    { return _each_price; }

    size_t getStolenQuantity() override
    { return _stolen_quantity; }
};

size_t IReportEntry::getTotalPrice()
{
    return getEachPrice() * getStolenQuantity();
}

typedef vector<shared_ptr<IRareCoin>> CoinsCollection;
typedef vector<shared_ptr<IReportEntry>> Report;

class ITheftReporter
{
public:
    virtual Report getReport(const CoinsCollection &coins) = 0;
};

class TheftReporter : public ITheftReporter
{
public:
    Report getReport(const CoinsCollection &coins) override;
};

Report TheftReporter::getReport(const CoinsCollection &coins)
{
    Report report;
    for (const auto &coin: coins)
    {
        if (coin->getRealQuantity() > coin->getDocumentedQuantity())
        {
            report.push_back(make_shared<ReportEntry>(coin->getName(), coin->getPrice(), coin->getRealQuantity() - coin->getDocumentedQuantity()));
        }
    }
    return report;
}

int main()
{
    CoinsCollection coins = {
            make_shared<RareCoin>("Penny", 1, 100, 99),
            make_shared<RareCoin>("Nickel", 5, 50, 50),
            make_shared<RareCoin>("Dime", 10, 100, 100),
            make_shared<RareCoin>("Quarter", 25, 25, 25),
            make_shared<RareCoin>("Half Dollar", 50, 10, 9),
            make_shared<RareCoin>("Dollar", 100, 5, 3)
    };

    TheftReporter reporter;
    Report report = reporter.getReport(coins);

    size_t sum = 0;
    cout << report.size() << " coins were stolen:" << endl;
    for (size_t i = 0; i < report.size(); ++i)
    {
        auto entry = report[i];
        cout << "\t" << i + 1 << ". " << entry->getStolenQuantity() << " " << entry->getName()
             << ", each worth " << entry->getEachPrice() << " (total worth "
             << entry->getTotalPrice() << ")" << endl;
        sum += entry->getTotalPrice();
    }
    cout << "Total worth: " << sum << endl;

    return 0;
}

#pragma clang diagnostic pop